import AuthHttp from "~/services/http-auth-service/auth.http";
export default (ctx, inject) => {
  const authInstance = new AuthHttp(ctx);

  ctx.$auth = authInstance;
  inject('auth', authInstance);
}
