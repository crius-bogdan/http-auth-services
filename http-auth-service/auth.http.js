import Storage from './auth.storage';
import authConfig from '@/auth.config';

export default class AuthHttp {
  constructor(context) {
    this.ctx = context;
    this.http = this.ctx.$http;

    this.storage = new Storage(this.ctx);

    this.storage.watchState('token', this.updateAuthorizationToken.bind(this));
  }

  get state() {
    return this.storage.state;
  }

  get loggedIn() {
    return this.state.loggedIn;
  }

  get user() {
    return this.state.user;
  }

  async checkAuthorization() {
    let cookieToken = this.storage.getCookie('token');
    if(cookieToken) {
      this.setStorageTokenSettings(cookieToken, true);
      this.updateAuthorizationToken(cookieToken);
      try {
        await this.fetchUser();
        console.log(this.loggedIn, 'first try')
      } catch (e) {
        try {
          await this.refreshToken();
          await this.fetchUser();
        } catch (e) {
          this.setStorageTokenSettings(null);
          this.setCookieTokenSettings(null);
        }
      }
    }
  }

  getTokenExpTime() {
    const dateNow = new Date();
    let time = dateNow.getTime();
    time += 60 * 60 * 1000;
    dateNow.setTime(time);

    return dateNow;
  }

  getTokenSettings(token, isCookie = false) {
    if(token) {
      const tokenType = authConfig.storage.tokenType;
      const tokenExpTime = this.getTokenExpTime();
      const tokenString = isCookie ? token : `${tokenType} ${token}`;
      return {
        token: tokenString,
        tokenExpTime
      };
    }
    return { token, tokenExpTime: null };
  }

  updateAuthorizationToken(value) {
    this.http.setHeader('Authorization', value);
  }

  setStorageTokenSettings(token, isCookie = false) {
    const tokenSettings = this.getTokenSettings(token, isCookie);
    this.storage.setStateField('token', tokenSettings.token);
    this.storage.setStateField('tokenExpTime', tokenSettings.tokenExpTime);
  }

  setCookieTokenSettings(token) {
    const tokenSettings = this.getTokenSettings(token);
    const tokenMaxAge = tokenSettings.tokenExpTime ? 3600 : 0;
    // debugger;
    this.storage.setCookie('token', tokenSettings.token, { maxAge: tokenMaxAge });
  }

  checkResponse(response, fail, success) {
    if(response.status !== 200) {
      this.setStorageTokenSettings(null);
      this.setCookieTokenSettings(null);
      fail(response);

      return;
    }

    const { body } = response;

    this.setStorageTokenSettings(body.token);
    this.setCookieTokenSettings(body.token);
    success();
  }

  login({ email, password }) {
    const queryString = authConfig.endpoints.login();

    return new Promise(async (resolve, reject) => {
      const response = await this.http.post(queryString, { body: { email, password } });

      this.checkResponse(response, reject, resolve);
    })
  }

  register(data) {
    const queryString = authConfig.endpoints.register();

    return new Promise(async (resolve, reject) => {
      const response = await this.http.post(queryString, { body: data });

      this.checkResponse(response, reject, resolve);
    });
  }

  logout() {
      const queryString = authConfig.endpoints.logout();

      return new Promise(async (resolve, reject) => {

        const response = await this.http.get(queryString);

        if(response.status !== 200) {
          reject(response);
          return;
        }

        this.setStorageTokenSettings(null);
        this.setCookieTokenSettings(null);
        this.storage.setStateField('loggedIn', false);

        resolve();
      });
  }

  deleteAccount() {

    return new Promise((resolve, reject) => {
      resolve();
    });

  }

  fetchUser() {

    const queryString = authConfig.endpoints.fetchUser();
    if(this.state.token) {
      return new Promise(async (resolve, reject) => {
        const response = await this.http.get(queryString);
        if(response.status !== 200) {
            reject(response);
            this.storage.setStateField('loggedIn', false);
            return;
        }

        const { body } = response;

        this.storage.setStateField('user', body);
        this.storage.setStateField('loggedIn', true);
        resolve();
      });

    }
  }

  refreshToken() {
    const queryString = authConfig.endpoints.tokenRefresh();


    return new Promise(async (resolve, reject) => {
      const response = await this.http.get(queryString);

      this.checkResponse(response, reject, resolve);
    });
  }

  async checkToken() {
    if(!this.state.token) return;

    const tokenExpTime = this.state.tokenExpTime;
    const dateNow = new Date();
    const dateCondition = dateNow > tokenExpTime;

    if(dateCondition) {
      try {
        await this.refreshToken();
      } catch (e) {
        return e;
      }
    }
  }
}
