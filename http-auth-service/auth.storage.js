import cookie from 'cookie';
import authConfig from '@/auth.config';
import { getProp, encodeValue, decodeValue, isUnsetValue } from '@/utils/auth.utils';

export default class Storage {
  constructor(context) {
    this.ctx = context;
    this.state = {};

    this._initState();
  }

  // Local state

  _initState() {
    this._canUseStore = !!this.ctx.store;

    if(this._canUseStore && !this.ctx.store.hasModule(authConfig.storage.moduleNamespace)) {
      // Default store module
      const storeModule = {
        namespaced: true,
        state: () => authConfig.storage.initStateInstance(),
        mutations: {
          SET(state, { key, value }) {
            state[key] = value;
          }
        }
      };
      // Register state module
      this.ctx.store.registerModule(authConfig.storage.moduleNamespace, storeModule, {
        preserveState: !!this.ctx.store.state[authConfig.storage.moduleNamespace]
      });

      this.state = this.ctx.store.state[authConfig.storage.moduleNamespace];
    } else {
      console.warn('Delete store or rename AUTH store');

      return;
    }
  }

  setStateField(key = '', value) {
    if(typeof key !== 'string' && key.length === 0) {
      return;
    }

    if(this._canUseStore) {
      this.ctx.store.commit(authConfig.storage.moduleNamespace + '/SET', {
        key,
        value
      })
    }
  }

  getStateFieldValue(key = '') {
    if(typeof key === 'string' && key.length !== 0) {
      return this.state[key];
    }
    return null;
  }

  watchState(key, fn) {
    if(this._canUseStore) {
      return this.ctx.store.watch(
        (state) => {
          return getProp(state[authConfig.storage.moduleNamespace], key)
        },
        fn
      )
    }
  }

  removeState(key) {
    this.setStateField(key, undefined)
  }

  // Cookies storage

  getAllCookies() {
    const cookieString = process.client ? document.cookie : this.ctx.req.headers.cookie;

    return cookie.parse(cookieString || '') || {};
  }

  setCookie(key, value, options = {}) {
    if(process.server && !this.ctx.req) {
      return
    }

    console.log(this.ctx.req)

    const _key = authConfig.storage.cookiesPrefix + key;
    const _options = Object.assign({}, options)
    const _value = encodeValue(value);

    if(isUnsetValue(value)) {
      _options.maxAge = -1;
    }

    const serializedCookie = cookie.serialize(_key, _value, _options);

    if(process.client) {
      document.cookie = serializedCookie;
    } else if (process.server && this.ctx.req) {

      const cookies = this.ctx.req.getHeader('Set-Cookie') || [];

      cookies.unshift(serializedCookie);

      cookies.filter((v, i, arr) => {
        return arr.findIndex((val) => {
          return val.startsWith(v.substr(0, v.indexOf('=')));
        }) === i;
      });

      return value;
    }
  }

  getCookie(key) {
    if(process.server && !this.ctx.req) return;

    const _key = authConfig.storage.cookiesPrefix + key;

    const cookies = this.getAllCookies();

    const value = cookies[_key] ? decodeURIComponent(cookies[_key]) : undefined;

    return decodeValue(value);
  }

  removeCookie(key, options = {}) {
    this.setCookie(key, undefined, options)
  }
};
